/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *    ClusterDistanceClassifier.java
 *    Copyright (C) 1999-2012 University of Waikato, Hamilton, New Zealand
 *
 */

package weka.classifiers.rules;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.Serializable;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Sourcable;
import weka.core.*;
import weka.core.Capabilities.Capability;

public class ClusterDistanceClassifier extends AbstractClassifier
    implements WeightedInstancesHandler, OptionHandler {

    /**
     * Represents a single source of location information, typically signal strength
     * Can be updated with new readings:
     *  https://math.stackexchange.com/questions/775391/
     *  https://math.stackexchange.com/questions/102978/
     */
    class Dimension implements Serializable {
        // TODO: store std_dev as well to avoid sqrt calculation
        public double mean, variance, count;

        @Override
        public String toString() {
            return "" + mean + " / " + Math.sqrt(variance) + " (" + count + ")";
        }
    }

    class Location implements Serializable {
        public final Map<Attribute, Dimension> dimensions = new HashMap<Attribute, Dimension>();

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Dimension dim : dimensions.values()) {
                sb.append(dim.toString()).append('\n');
            }
            return sb.toString();
        }
    }

    private int m_ClassIndex = 0;
    private double m_DistFactor = 2.0;

    private Location m_Cluster;

    /** The class attribute. Ignored in input, only used for output. */
    private Attribute m_ClassAttr;
    private double m_DistCutoff;

    /**
     * Returns default capabilities of the classifier.
     *
     * @return the capabilities of this classifier
     */
    @Override
    public Capabilities getCapabilities() {
        Capabilities result = super.getCapabilities();
        result.disableAll();

        // attributes
        result.enable(Capability.NOMINAL_ATTRIBUTES);
        result.enable(Capability.NUMERIC_ATTRIBUTES);
        result.enable(Capability.DATE_ATTRIBUTES);
        result.enable(Capability.STRING_ATTRIBUTES);
        result.enable(Capability.RELATIONAL_ATTRIBUTES);
        result.enable(Capability.MISSING_VALUES);

        // class
        result.enable(Capability.NOMINAL_CLASS);
        result.enable(Capability.NUMERIC_CLASS);
        result.enable(Capability.DATE_CLASS);
        result.enable(Capability.MISSING_CLASS_VALUES);

        // instances
        result.setMinimumNumberInstances(0);

        return result;
    }

    /**
     * Generates the classifier.
     *
     * @param instances set of instances serving as training data
     * @throws Exception if the classifier has not been generated successfully
     */
    @Override
    public void buildClassifier(Instances instances) throws Exception {
        // can classifier handle the data?
        getCapabilities().testWithFail(instances);

        m_ClassAttr = instances.classAttribute();
        m_Cluster = new Location();

        Enumeration<Attribute> attrs = instances.enumerateAttributes();
        while (attrs.hasMoreElements()) {
            Attribute attr = attrs.nextElement();
            Dimension dim = new Dimension();
            double total = 0;
            for (Instance instance : instances) {
                if (instance.value(m_ClassAttr) != m_ClassIndex || instance.isMissing(attr)) {
                    continue;
                } else {
                    total += instance.value(attr);
                    dim.count += 1;
                }
            }
            dim.mean = total / dim.count;
            dim.variance = 0;
            for (Instance instance : instances) {
                if (instance.value(m_ClassAttr) != m_ClassIndex || instance.isMissing(attr)) {
                    continue;
                } else {
                    double xMinusMean = instance.value(attr) - dim.mean;
                    dim.variance += (xMinusMean * xMinusMean);
                }
            }
            if (dim.count > 1) {
                dim.variance /= (dim.count - 1);
                m_Cluster.dimensions.put(attr, dim);
            }
        }

        // calculate the dist cutoff value
        double totalDist = 0;
        double count = 0;
        for (Instance instance : instances) {
            if (instance.value(m_ClassAttr) == m_ClassIndex) {
                totalDist += dist(instance);
                count += 1;
            }
        }
        double meanDist = totalDist / count;
        double varianceDist = 0;
        for (Instance instance : instances) {
            if (instance.value(m_ClassAttr) == m_ClassIndex) {
                double xMinusMean = dist(instance) - meanDist;
                varianceDist += (xMinusMean * xMinusMean);
            }
        }
        double stdDevDist = Math.sqrt(varianceDist / (count - 1));
        m_DistCutoff = meanDist + stdDevDist * m_DistFactor;
    }

    public static double erf(double z) {
        double t = 1.0 / (1.0 + 0.5 * Math.abs(z));

        // use Horner's method
        double ans = 1 - t * Math.exp(-z*z - 1.26551223 +
                                       t * ( 1.00002368 +
                                       t * ( 0.37409196 +
                                       t * ( 0.09678418 +
                                       t * (-0.18628806 +
                                       t * ( 0.27886807 +
                                       t * (-1.13520398 +
                                       t * ( 1.48851587 +
                                       t * (-0.82215223 +
                                       t * ( 0.17087277))))))))));
        if (z >= 0) return  ans;
        else        return -ans;
    }

    public static double normalise(double x, double mean, double stdDev) {
        return (x - mean) / stdDev;
    }

    public static double err(double x, double mean, double stdDev) {
        return erf(Math.abs(normalise(x, mean, stdDev)));
    }

    /**
     * Calculate the distance of an instance from the cluster
     */
    public double dist(Instance instance) {
        double dist = 0;
        Enumeration<Attribute> attrs = instance.enumerateAttributes();
        while (attrs.hasMoreElements()) {
            Attribute attr = attrs.nextElement();
            Dimension dim = m_Cluster.dimensions.get(attr);
            double value;
            if (instance.isMissing(attr)) {
                value = -100;
            } else {
                value = instance.value(attr);
            }
            double delta = 0;
            if (dim == null) {
                delta = Math.sqrt(100.0 + value);
            } else {
                double attrErr = err(value, dim.mean, Math.sqrt(dim.variance));
                // the following is somewhat arbitrary based on what worked well
                double sqrtCount = Math.log(dim.count);
                double attrDist = attrErr * sqrtCount;
                // System.out.print("normal path: value=" + value + ", mean=" + dim.mean + ", stdDev=" + Math.sqrt(dim.variance) + ", attrErr=" + attrErr + ", sqrtCount=" + sqrtCount + ", attrDist=" + attrDist);
                delta = Math.sqrt(attrDist);
            }
            // System.out.println(attr.toString() + " delta is " + delta);
            if (!Double.isNaN(delta)) {
                dist += delta;
            }
        }
        return dist;
    }

    /**
     * Classifies a given instance.
     *
     * @param instance the instance to be classified
     * @return index of the predicted class
     */
    @Override
    public double classifyInstance(Instance instance) {
        if (dist(instance) < m_DistCutoff) {
            return m_ClassIndex;
        } else {
            return Math.abs(m_ClassIndex - 1);
        }
    }

    /**
     * Returns a description of the classifier.
     *
     * @return a description of the classifier as a string.
     */
    @Override
    public String toString() {
        return "Class: " + m_ClassAttr.toString() + "\n"
            + "Dist Cutoff: " + m_DistCutoff + "\n";
            // + m_Cluster.toString();
    }

    @Override
    public Enumeration<Option> listOptions() {
        java.util.Vector<Option> newVector = new java.util.Vector<Option>();
        newVector.addElement(new Option("\tSet the class value to predict\t(default = 0)",
                                        "C", 1, "-C <class id>"));
        newVector.addElement(new Option("\tSet the distance factor in standard deviations from the mean distance in the cluster\t(default = 2)",
                                        "D", 1, "-D <dist>"));
        newVector.addAll(Collections.list(super.listOptions()));
        return newVector.elements();
    }

    @Override
    public String[] getOptions() {
        java.util.Vector<String> result = new java.util.Vector<String>();
        result.add("-C");
        result.add(Integer.toString(m_ClassIndex));
        result.add("-D");
        result.add(Double.toString(m_DistFactor));

        Collections.addAll(result, super.getOptions());
        return result.toArray(new String[result.size()]);
    }

    @Override
    public void setOptions(String[] options) throws Exception {
        String classString = Utils.getOption('C', options);
        if (classString.length() != 0) {
            m_ClassIndex = Integer.parseInt(classString);
        }
        String distFactorString = Utils.getOption('D', options);
        if (distFactorString.length() != 0) {
            m_DistFactor = Double.parseDouble(distFactorString);
        }
        super.setOptions(options);
        Utils.checkForRemainingOptions(options);
    }

    /**
     * Main method for testing this class.
     *
     * @param argv the options
     */
    public static void main(String[] argv) {
        runClassifier(new ClusterDistanceClassifier(), argv);
    }
}
